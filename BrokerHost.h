

#include <QString>
#include <QFile>
#include <QDir>
#include <QList>
#include "ui_ConquestBroker.h"
#include <QObject>
//#include <qapplication.h>
#include <iostream>
#include <vector>
#include <sstream>
#include <QTextStream>
#include <QFileInfo>
#include <QDomDocument>
#include <QHeaderView>
#include <QToolButton>
#include <QTabWidget>
#include <QSqlQuery>
#include <QItemSelectionModel>
  
#include <QtSql>
#include <QMessageBox>
#include <stdlib.h>
#include <time.h>
#include <QtGlobal>
#include <QTextStream>

#ifndef __BrokerHost_H__
#define __BrokerHost_H__

#include "BrokerApplication.h"
#include "ReadOnlySqlTableModel.h"

class BrokerHost: public QObject, public BrokerQueryHost { // Should rename to host?

	Q_OBJECT

	public:

	BrokerHost(QDomElement conf); 
	BrokerHost(QDomElement conf, Ui::ConquestBroker ui);	
	~BrokerHost();
	static QString getXMLProperty(QDomElement xml, QString name, QString defaultProperty);
	static int getXMLProperty(QDomElement xml, QString name, int defaultProperty);
	static QList<QString> getXMLProperties(QDomElement xml, QString name);
	static void setTableHeader(QDomElement *tableConf, QSqlTableModel *tableModel, QTableView *tableView);
	static BrokerHost* readConfig(QString filename,  Ui::ConquestBroker ui);
	void addApplications(QDomElement *applicationConf, QWidget *panel, QGridLayout *gridLayout);
	BrokerApplication launch(BrokerApplication app); // Spawn an instance of this application
	BrokerApplication attach(); // Allow a separate process to attach to a spawned application
//	QSqlTableModel *seriesModel, *studyModel, *patientModel;

	QList<QString> getSelectionAsFiles();
	QDir getNewOutputDir();
	QDir getNewTmpDir();
	void notifyOutputAvailable(QDir outputDir);

	private:
		bool hasUI;
		QSqlDatabase db;
		QDir conquest_dir, device_dir, broker_dir;
		QString series_query_table, study_query_table, patient_query_table;
		QString series_query_images, study_query_images, patient_query_images; // SQL query to get images
		QList<QString> series_update_table, study_update_table, patient_update_table; // SQL query to update intermediate tables
        QList<QString> series_create_table, study_create_table, patient_create_table; // SQL drop and create intermediate tables
        QSqlTableModel *series_model, *study_model, *patient_model;
		QTableView *series_view, *study_view, *patient_view;
		QList<BrokerApplication*> applications;
		QTabWidget *view_tab;		
		QListWidget *log_items;
//	    QStatusBar *statusbar;

	public slots:
        bool initialiseTables();
        bool updateModels();
		void setStatus(QString status);

	signals:
		void messageChanged(const QString &message);

};

#endif

