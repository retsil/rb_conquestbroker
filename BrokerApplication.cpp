
#include "BrokerApplication.h"
#include "BrokerHost.h"
//#include <unistd.h>
BrokerApplication::BrokerApplication(BrokerQueryHost *s_qhost, QDomElement xmlEl, QToolButton *button) {
    enabled = BrokerHost::getXMLProperty(xmlEl,"enabled","no").contains("yes", Qt::CaseInsensitive);
    name = BrokerHost::getXMLProperty(xmlEl,"name","");
    description = BrokerHost::getXMLProperty(xmlEl,"description","");
    icon_path = BrokerHost::getXMLProperty(xmlEl,"icon","");
    exe_path = BrokerHost::getXMLProperty(xmlEl,"exe","");
    button->setToolTip(name);
    button->setStatusTip(description);
    button->setText(name);
    button->setEnabled(enabled);
    button->setIcon(QIcon(icon_path));
    qhost = s_qhost;
    if (QObject::connect(button, SIGNAL(clicked(bool)),
                          this, SLOT(button_clicked(bool))))
        qhost -> setStatus(QString("Slot connected"));
    qDebug( qhost -> getNewTmpDir().absolutePath().toLatin1().constData());
}

BrokerApplication::BrokerApplication(BrokerApplication *src) {
    enabled = src -> isEnabled();
    name = src -> getName();
    description = src -> getDescription();
    icon_path = src -> getIconPath();
    exe_path = src -> getExePath();
    qhost = src -> getQueryHost();
    tmp_dir = qhost -> getNewTmpDir();
    output_dir = qhost -> getNewOutputDir();
    input_filenames = qhost -> getSelectionAsFiles();
    for (QList<QString>::iterator i = input_filenames.begin(); i != input_filenames.end(); ++i) qhost -> setStatus(*i);
}

void BrokerApplication::button_clicked(bool state) {
    if (!state) {
        BrokerApplication *app = new BrokerApplication(this);
        qhost -> setStatus(QString("button pressed"));
        app -> start();
    }
}

void BrokerApplication::run() {
    qhost -> setStatus(QString("Application starting"));
    qhost -> setStatus(QString(getExePath().toLatin1().constData()));
    QString cwd = QDir::currentPath();

    if (! tmp_dir.exists()) {
        qhost -> setStatus(QString("Tmp dir non existent\n"));
        return;
    }

    if (! output_dir.exists()) {
        qhost -> setStatus(QString("Output dir non existent\n"));
        return;
    }

    if (! QDir::setCurrent(tmp_dir.absolutePath())) {
        qhost -> setStatus(QString("Tmp dir not accessible\n"));
        return;
    }
    QFile fileList(tmp_dir.absoluteFilePath("fileList.txt"));
    if (!fileList.open(QIODevice::WriteOnly | QIODevice::Text)) {
        qhost -> setStatus(QString("Could not open file list\n"));
        return;
    }
    QTextStream out(&fileList);
    //out << output_dir.absolutePath() << "\n";
    for (QList<QString>::iterator f = input_filenames.begin(); f != input_filenames.end(); ++f) {
        QString img_path = *f;
        #ifndef Q_WS_WIN
        img_path = img_path.replace(QString("/"), QString("\\"));
        #endif
        out << img_path << "\n";
    }
    fileList.close();

    //tmp_dir.mkdir(tmp_dir);
    //output_dir.mkdir(output_dir);
    //system(tmp_dir.absolutePath().prepend("mkdir ").toLatin1().constData());
    //system(output_dir.absolutePath().prepend("mkdir ").toLatin1().constData());
    //system(tmp_dir.absolutePath().prepend("cd ").toLatin1().constData());

    #ifdef Q_WS_WIN
    //int BUFSIZE=1024;
    //TCHAR chNewEnv[BUFSIZE];
    //LPTSTR lpszCurrentVariable;
    const TCHAR BROKER_INPUT[] = L"BROKER_INPUT";
    const TCHAR BROKER_OUTPUT[] = L"BROKER_OUTPUT";

    //DWORD dwFlags=0;
    //TCHAR szAppName[]=TEXT("ex3.exe");
    //STARTUPINFO si;
    //PROCESS_INFORMATION pi;
    //BOOL fSuccess;

     // Copy environment strings into an environment block.
    //lpszCurrentVariable = (LPTSTR) chNewEnv;
    //lpszCurrentVariable = wcsncat(lpszCurrentVariable, BROKER_INPUT, lstrlen(BROKER_INPUT));
    //lpszCurrentVariable = wcsncat(lpszCurrentVariable, tmp_dir.absoluteFilePath("fileList.txt").toStdWString().c_str(), tmp_dir.absoluteFilePath("fileList.txt").toStdWString().length());
    //lpszCurrentVariable += lstrlen(lpszCurrentVariable) + 1;
    //lpszCurrentVariable = wcsncat(lpszCurrentVariable, BROKER_OUTPUT, lstrlen(BROKER_OUTPUT));
    //lpszCurrentVariable = wcsncat(lpszCurrentVariable, output_dir.absolutePath().toStdWString().c_str(), output_dir.absolutePath().toStdWString().length());
    // Terminate the block with a NULL byte.
    //lpszCurrentVariable += lstrlen(lpszCurrentVariable) + 1;
    //*lpszCurrentVariable = (TCHAR)0;

    SetEnvironmentVariable(BROKER_INPUT, tmp_dir.absoluteFilePath("fileList.txt").toStdWString().c_str());
    SetEnvironmentVariable(BROKER_OUTPUT, output_dir.absolutePath().toStdWString().c_str());


    STARTUPINFO si;
    PROCESS_INFORMATION pi;
    WCHAR* cmd = new WCHAR[getExePath().toStdWString().length()+1];
    cmd = wcsncpy(cmd,getExePath().toStdWString().c_str(), getExePath().toStdWString().length()+1);
    ZeroMemory( &si, sizeof(si) );
    si.cb = sizeof(si);
    ZeroMemory( &pi, sizeof(pi) );
    if (CreateProcess(NULL, cmd, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi)) {
        // Wait until child processes exit.
        WaitForSingleObject( pi.hProcess, INFINITE );
        // Close process and thread handles.
        CloseHandle( pi.hProcess );
        qhost -> setStatus(QString("Windows Application complete"));
    } else {
        qhost -> setStatus(QString("Windows Application fail"));
        qhost -> setStatus(getExePath());
        //	qDebug(cmd);
    }
    #endif
    #ifndef Q_WS_WIN
    if (system(getExePath().toLatin1().constData()) == 0)  {
        qhost -> setStatus(QString("Application complete"));
    } else {
        qhost -> setStatus(QString("Application fail"));
    }
    #endif
    fileList.remove();
    QDir::setCurrent(cwd);
    qhost -> notifyOutputAvailable(output_dir);
    tmp_dir.rmpath(".");
    output_dir.rmpath(".");
    //system(tmp_dir.absolutePath().prepend("rmdir ").toLatin1().constData());
    //system(output_dir.absolutePath().prepend("rmdir ").toLatin1().constData());

}

bool BrokerApplication::isEnabled() {
	return(enabled);
}

const QString BrokerApplication::getName() {
	return(name);
}

const QString BrokerApplication::getDescription() {
	return(description);
}

QString BrokerApplication::getIconPath() {
	return(icon_path);
}

QString BrokerApplication::getExePath() {
	return(exe_path);
}


BrokerQueryHost* BrokerApplication::getQueryHost() {
	return(qhost);
}

