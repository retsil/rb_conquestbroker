
#include "ReadOnlySQLTableModel.h"

Qt::ItemFlags ReadOnlySqlTableModel::flags(const QModelIndex &index) const {
	return Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsUserCheckable;
}

//Qt::ItemFlags ReadOnlySqlTableModel::flags( QModelIndex & index ) const {
//	return Qt::ItemIsSelectable;
//	//	return QSqlTableModel::flags(index);
//}

