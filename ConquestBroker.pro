# -*- mode: sh -*- ################################################
# Qwt Widget Library
# Copyright (C) 1997   Josef Wilgen
# Copyright (C) 2002   Uwe Rathmann
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the Qwt License, Version 1.0
###################################################################

include( examples.pri )

TARGET  = ConquestBroker

HEADERS = \
    ReadOnlySqlTableModel.h \
    BrokerUi.h \
    BrokerHost.h  \ 
    BrokerApplication.h

SOURCES = \
    ReadOnlySqlTableModel.cpp \
    BrokerUi.cpp \
    BrokerHost.cpp \
    BrokerApplication.cpp

FORMS       = ConquestBroker.ui

TEMPLATE = app

QT += widgets
QT += xml
QT += sql

INCLUDEPATH+="C:\Program Files\PostgreSQL\9.3\include"
LIBS+=-L"C:\Program Files\PostgreSQL\9.3\lib" -llibpq
 #LIBS+=-L"E:\Qt\2009.02\qt\plugins\sqldrivers" -lqsqlpsql4


# LIBS+=-L/opt/local/lib/postgresql82 -lpq

# INCLUDEPATH+=/usr/include

# INCLUDEPATH+=/opt/local/include/postgresql82

#LIBS+=-lqsqlite4 -L.

# -L"E:\Qt\2009.02\qt\plugins\sqldrivers"
