
#include "BrokerHost.h"
#include "BrokerUi.h"

BrokerHost* BrokerHost::readConfig(QString filename,  Ui::ConquestBroker ui) {
	qDebug() << "Opening xml file";
	QDomDocument doc("config");
	QFile file(filename);
	if (!file.open(QIODevice::ReadOnly)) {
		QMessageBox::critical(0, "Cannot open config",
					"Cannot open config xml file for read only access.\n",
					QMessageBox::Close);
		exit(1);
	}
    QString errorMsg;
    int errorLine = 0;
    int errorColumn = 0;
    if (!doc.setContent(&file, false, &errorMsg, &errorLine, &errorColumn)) {
        qDebug() << "Error parsing " << filename << " " << errorMsg;
        qDebug() << "Line: " << errorLine << " Column: " << errorColumn;
        QMessageBox::critical(0, "Cannot open xml config",
                    errorMsg,
					QMessageBox::Close);
		file.close();
		exit(1);
	}
	file.close();
	QDomElement docElem = doc.documentElement();
	return(new BrokerHost(docElem,  ui));
}


BrokerHost::BrokerHost(QDomElement conf, Ui::ConquestBroker ui) {

	view_tab = ui.tabWidget;
	series_view = ui.seriesView;
	study_view = ui.studyView;
	patient_view = ui.patientView;
	log_items = ui.logItems;

	QDomElement configEl = conf.elementsByTagName("config").at(0).toElement();

	QDomElement filesystemEl = conf.elementsByTagName("filesystem").at(0).toElement();
	conquest_dir = BrokerHost::getXMLProperty(filesystemEl,"conquestdir","");
	device_dir = BrokerHost::getXMLProperty(filesystemEl,"devicedir","");
	broker_dir = BrokerHost::getXMLProperty(filesystemEl,"brokerdir","");
    qDebug() << "Broker dir " << broker_dir.absolutePath() << "\n";
    qDebug() << "Broker dir " <<  BrokerHost::getXMLProperty(filesystemEl,"brokerdir","") << "\n";
	

	QDomElement databaseEl = conf.elementsByTagName("database").at(0).toElement();
	QString driver = getXMLProperty(databaseEl,"driver","QSQLITE");

	db = QSqlDatabase::addDatabase(driver);
    db.setDatabaseName(getXMLProperty(databaseEl,"databaseName",db.databaseName()));
    db.setPort(getXMLProperty(databaseEl,"port",db.port()));
    db.setHostName(getXMLProperty(databaseEl,"hostName",db.hostName()));
    db.setUserName(getXMLProperty(databaseEl,"userName",db.userName()));
    db.setPassword(getXMLProperty(databaseEl,"password",db.password()));

	QDomElement seriesEl = conf.elementsByTagName("series").at(0).toElement();
	series_query_table = getXMLProperty(seriesEl,"queryTable","");
	series_query_images = getXMLProperty(seriesEl,"queryImages","");
	series_update_table = getXMLProperties(seriesEl,"updateTable");
    series_create_table = getXMLProperties(seriesEl,"createTable");

	QDomElement studyEl = conf.elementsByTagName("study").at(0).toElement();
	study_query_table = getXMLProperty(studyEl,"queryTable","");
	study_query_images = getXMLProperty(studyEl,"queryImages","");
	study_update_table = getXMLProperties(studyEl,"updateTable");
    study_create_table = getXMLProperties(studyEl,"createTable");

	QDomElement patientEl = conf.elementsByTagName("patient").at(0).toElement();
	patient_query_table = getXMLProperty(patientEl,"queryTable","");
	patient_query_images = getXMLProperty(patientEl,"queryImages","");
	patient_update_table = getXMLProperties(patientEl,"updateTable");
    patient_create_table = getXMLProperties(patientEl,"createTable");

	QDomElement clinicalEl = conf.elementsByTagName("clinical").at(0).toElement();
	QDomElement researchEl = conf.elementsByTagName("research").at(0).toElement();
	QDomElement developmentEl = conf.elementsByTagName("development").at(0).toElement();

	addApplications(&clinicalEl , ui.clinical, ui.gridLayout);
	addApplications(&researchEl , ui.research, ui.gridLayout_2);
	addApplications(&developmentEl , ui.development, ui.gridLayout_3);

    if (!db.open()) {
		QMessageBox::critical(0, "Cannot open database",
					  "Unable to establish a database connection.\n" +
					  db.lastError().driverText(),
					  QMessageBox::Ok);
    };

	series_model = new ReadOnlySqlTableModel();
	study_model = new ReadOnlySqlTableModel();
	patient_model = new ReadOnlySqlTableModel();

	 patient_model->setTable(patient_query_table);
     patient_model->select();
//     patient_model->removeColumn(0); // don't show the ID

	 study_model->setTable(study_query_table);
     study_model->select();
//     study_model->removeColumn(0); // don't show the ID

	 series_model->setTable(series_query_table);
     series_model->select();
//     series_model->removeColumn(0); // don't show the ID

	ui.patientView->setModel(patient_model);
	ui.patientView->show();
	
	ui.studyView->setModel(study_model);
	ui.studyView->show();

	ui.seriesView->setModel(series_model);
	ui.seriesView->show();

	BrokerHost::setTableHeader(&patientEl, patient_model, ui.patientView);
	BrokerHost::setTableHeader(&studyEl, study_model, ui.studyView);
	BrokerHost::setTableHeader(&seriesEl, series_model, ui.seriesView);


	if (QObject::connect(this, SIGNAL(messageChanged(QString)),
                      ui.statusbar, SLOT(showMessage(QString)))) {
        setStatus(QString("Slot to show messages is connected"));
    }

	if (QObject::connect(ui.actionRefresh, SIGNAL(triggered()),
                      this, SLOT(updateModels()))) {
        setStatus(QString("Slot to refresh tables is connected"));
    }

    if (QObject::connect(ui.actionInitialise_Tables, SIGNAL(triggered()),
                     this, SLOT(initialiseTables()))) {
        setStatus(QString("Slot to initialise tables is connected"));
    }

//	if (QObject::connect(ui.actionQuit, SIGNAL(triggered()),
 //                     ui, SLOT(close())))

    if (updateModels()) {
        ui.actionRefresh->setEnabled(true);
    } else {
        ui.actionInitialise_Tables->setEnabled(true);
    }
}

bool BrokerHost::initialiseTables() {
    bool hasError = false;
    setStatus(QString("Initialising tables"));
    QList<QString>::iterator i;
    for (i = patient_create_table.begin(); i != patient_create_table.end(); ++i) {
        setStatus(i -> trimmed());
        QSqlQuery query;
        if (! query.exec(i -> trimmed())) {
            setStatus("Database error Initialising table");
            setStatus(query.lastError().databaseText());
            hasError = true;
        }
    }
    for (i = study_create_table.begin(); i != study_create_table.end(); ++i) {
        setStatus(i -> trimmed());
        QSqlQuery query;
        if (! query.exec(i -> trimmed())) {
            setStatus("Database error Initialising table");
            setStatus(query.lastError().databaseText());
            hasError = true;
        }
    }
    for (i = series_create_table.begin(); i != series_create_table.end(); ++i) {
        setStatus(i -> trimmed());
        QSqlQuery query;
        if (! query.exec(i -> trimmed())) {
            setStatus("Database error Initialising table");
            setStatus(query.lastError().databaseText());
            hasError = true;
        }
    }
    if (hasError) {
        setStatus(QString("Create failed"));
     } else {
        setStatus(QString("Tables created. Please restart the conquest broker."));
    }
    return(! hasError);
}

bool BrokerHost::updateModels() {
    bool hasError = false;
    setStatus(QString("Updating models"));
	QList<QString>::iterator i;
	for (i = patient_update_table.begin(); i != patient_update_table.end(); ++i) {
		setStatus(i -> trimmed());
        QSqlQuery query;
		if (! query.exec(i -> trimmed())) {
			setStatus("Database error updating table");
			setStatus(query.lastError().databaseText());
            hasError = true;
		}
	}
	for (i = study_update_table.begin(); i != study_update_table.end(); ++i) {
		setStatus(i -> trimmed());
        QSqlQuery query;
		if (! query.exec(i -> trimmed())) {
			setStatus("Database error updating table");
			setStatus(query.lastError().databaseText());
            hasError = true;
        }
	}
	for (i = series_update_table.begin(); i != series_update_table.end(); ++i) {
		setStatus(i -> trimmed());
        QSqlQuery query;
		if (! query.exec(i -> trimmed())) {
			setStatus("Database error updating table");
			setStatus(query.lastError().databaseText());
            hasError = true;
        }
	}

	series_model -> select();
	study_model -> select();
	patient_model -> select();
	patient_view -> show();
	study_view -> show();	
	series_view -> show();
    if (hasError) {
        setStatus(QString("Update failed"));
    } else {
        setStatus(QString("Models updated"));
    }
    return(! hasError);
}

BrokerHost::~BrokerHost() {
	db.close();
	for (QList<BrokerApplication*>::iterator i = applications.begin(); i != applications.end(); ++i) delete *i;
	delete series_model;
	delete study_model;
	delete patient_model;
}

void BrokerHost::setTableHeader(QDomElement *tableConf, QSqlTableModel *queryModel, QTableView *tableView) {
	QDomNodeList columnList = tableConf -> elementsByTagName("column");
	for (int i = 0; i < columnList.count(); i++) {
		queryModel-> setHeaderData(i, Qt::Horizontal, getXMLProperty(columnList.at(i).toElement(),"name",""));
		tableView -> setColumnWidth(i, getXMLProperty(columnList.at(i).toElement(),"width",100));
	}
}

void BrokerHost::addApplications(QDomElement *applicationConf, QWidget *panel, QGridLayout *gridLayout) {
	QDomNodeList applicationList = applicationConf -> elementsByTagName("application");
	for (int index=0; index < applicationList.count(); index++) {
		QToolButton *applicationIcon1 = new QToolButton(panel);
        applicationIcon1->setMinimumSize(QSize(64, 64));
        applicationIcon1->setIconSize(QSize(64, 64));
        applicationIcon1->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        gridLayout->addWidget(applicationIcon1, index / 2, index % 2, 1, 1);
		applications.append(new BrokerApplication(this, applicationList.at(index).toElement(), 
			applicationIcon1));
	}
}

QList<QString> BrokerQueryHost::getSelectionAsFiles() {
	return QList<QString>();
}

QDir BrokerQueryHost::getNewOutputDir() {
	return QDir(); 
}

QDir BrokerQueryHost::getNewTmpDir() {
	return QDir(); 
}

void BrokerQueryHost::notifyOutputAvailable(QDir outputDir) {
}

void BrokerQueryHost::setStatus(QString status) {
}

QList<QString> BrokerHost::getSelectionAsFiles() {
	QList<QString> files;

	switch (view_tab -> currentIndex()) {
		case 0: { // Patient
            QSqlQuery query;
            QModelIndexList rows = patient_view -> selectionModel() -> selectedRows();
            qDebug() << "Reading patient selection\n";
			for (QModelIndexList::iterator i = rows.begin(); i != rows.end(); ++i) {
                QString patient_uid = (i -> data()).toString();
                qDebug() << "Patient UID: " << patient_uid << "\n";
                qDebug() << patient_query_images << "\n";
				query.prepare(patient_query_images);
				query.addBindValue(patient_uid);
				query.exec();
				while (query.next()) {
					files.append(device_dir.absoluteFilePath(query.value(0).toString()));
				}
			}
			break;
		}
		case 1: { // Study
			 QSqlQuery query;
			 QModelIndexList rows = study_view -> selectionModel() -> selectedRows();
             qDebug() << "Reading patient selection\n";
             for (QModelIndexList::iterator i = rows.begin(); i != rows.end(); ++i) {
				QString study_uid = (i -> data()).toString();
                qDebug() << "Study UID: " << study_uid << "\n";
                qDebug() << patient_query_images << "\n";
                query.prepare(study_query_images);
				query.addBindValue(study_uid);
				query.exec();
				while (query.next()) {
					files.append(device_dir.absoluteFilePath(query.value(0).toString()));
				}
			}
			
			break;
		}
		case 2: { // Series
            QSqlQuery query;
            QModelIndexList rows = series_view -> selectionModel() -> selectedRows();
            qDebug() << "Reading patient selection\n";
			for (QModelIndexList::iterator i = rows.begin(); i != rows.end(); ++i) {
				QString series_uid = (i -> data()).toString();
                qDebug() << "Study UID: " << series_uid << "\n";
                qDebug() << patient_query_images << "\n";
                query.prepare(series_query_images);
				query.addBindValue(series_uid);
				query.exec();
				while (query.next()) {
					files.append(device_dir.absoluteFilePath(query.value(0).toString()));
				}
			}
			break;
		}
	}

	return files; 
}

QDir BrokerHost::getNewOutputDir() {
	QString dirName = "BrokerOutput" + QString::number(rand(), 16).toUpper();
	if (broker_dir.mkdir(dirName)) {
		return broker_dir.absoluteFilePath(dirName);     
	} else {
		return QDir();
	}
}

QDir BrokerHost::getNewTmpDir() {
	QString dirName = "BrokerTmp" + QString::number(rand(), 16).toUpper();
	if (broker_dir.mkdir(dirName)) {
		return broker_dir.absoluteFilePath(dirName);     
	} else {
		return QDir();
	}
}

void BrokerHost::notifyOutputAvailable(QDir outputDir) {

QFile dgateCommandsFile(outputDir.absoluteFilePath("dgateCommands.txt"));
QList<QString> *dgateCommands = new QList<QString>();
if (dgateCommandsFile.exists() & dgateCommandsFile.open(QIODevice::ReadOnly)) {
	QTextStream stream(&dgateCommandsFile);
	QString line;
	do {
     line = stream.readLine();
	 dgateCommands -> append(line);
	} while (!line.isNull());		
//	stream.close();
	dgateCommandsFile.close();
	dgateCommandsFile.remove();
}

outputDir.setFilter(QDir::Files);
QFileInfoList list = outputDir.entryInfoList();
if (! conquest_dir.exists()) { 
	setStatus(QString("Conquest dir nonexistent"));
	return;
}
if (! QDir::setCurrent(conquest_dir.absolutePath())) {
	setStatus(QString("Conquest dir not accessible\n"));
	return;
}
#ifdef Q_WS_WIN
	QString dgate_path = conquest_dir.absoluteFilePath("dgate.exe");
#endif
#ifndef Q_WS_WIN
	QString dgate_path = conquest_dir.absoluteFilePath("dgate");
#endif

if (! QFile::exists(dgate_path)) {
	setStatus(QString("dgate not found"));
	return;
}
for (int i = 0; i < list.size(); ++i) {
	QFileInfo fileInfo = list.at(i);		
    QString cmd = dgate_path.append(" --addimagefile:").append(fileInfo.absoluteFilePath());
    #ifdef Q_WS_WIN
        cmd = cmd.replace(QString("/"), QString("\\"));
    #endif
    if (system(cmd.toLatin1().data()) == 1) {
		QFile f(fileInfo.absoluteFilePath());
		setStatus(fileInfo.absoluteFilePath().append(" has been imported."));
//			f.remove();
	}
}
for (int i = 0; i < dgateCommands -> size(); ++i) {
	setStatus(dgateCommands -> at(i));
    if (system(dgate_path.append(" ").append(dgateCommands -> at(i)).toLatin1().data()) == 1) {
		setStatus("Command successful");
	} else {
		setStatus("Command failed");
	}
}
delete dgateCommands;
}

void BrokerHost::setStatus(QString status) {
	log_items -> addItem(status);
	messageChanged(status);
}


	
int BrokerHost::getXMLProperty(QDomElement xml, QString name, int defaultProperty) {
	QString svalue = BrokerHost::getXMLProperty(xml, name, "");
	bool ok;
	int value = svalue.toInt(&ok,10);
	if (ok) {
		return(value);
	} else {
		return(defaultProperty);
	}
}

QString BrokerHost::getXMLProperty(QDomElement xml, QString name, QString defaultProperty) {
	QDomNodeList list =  xml.elementsByTagName(name);
	if (list.isEmpty()) { return(defaultProperty); };
	if (! list.at(0).isElement()) { return(defaultProperty); };
	QDomElement el = list.at(0).toElement();
	if (! el.hasChildNodes()) { return(defaultProperty); };
	if (! el.firstChild().isText()) { return(defaultProperty); };
	return(el.firstChild().toText().data());
}


QList<QString> BrokerHost::getXMLProperties(QDomElement xml, QString name) {
	QList<QString> values;
	QDomNodeList list =  xml.elementsByTagName(name);
	if (list.isEmpty()) { return(values); };
	for (int i=0; i < list.count(); i++) {
		if (list.at(i).isElement()) {
			QDomElement el = list.at(i).toElement();
			if (el.hasChildNodes() && el.firstChild().isText()) {
				values.append(el.firstChild().toText().data());
			}
		}
	}
	return values;
}




int main (int argc, char **argv)
{

	/* initialize random seed: */
	srand ( time(NULL) );
  
    QApplication app(argc, argv);
	
	QMainWindow *widget = new QMainWindow();
	BrokerUi ui;
	ui.setupUi(widget);
//    QGridLayout *gridLayout = new QGridLayout(ui.Plot);
	BrokerHost *b = BrokerHost::readConfig("ConquestBrokerConfig.xml", ui);

	if (QObject::connect(ui.actionQuit, SIGNAL(triggered()),
                     widget, SLOT(close())))

	widget->show();
	int result = app.exec();
	delete b;
	return(result);
}

