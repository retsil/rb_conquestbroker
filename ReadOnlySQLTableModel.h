
#include <QtSql>

class ReadOnlySqlTableModel: public QSqlTableModel {
//	ReadOnlySqlTableModel
	public:
		Qt::ItemFlags flags(const QModelIndex &index) const;
};
