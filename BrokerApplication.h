


#include <QString>
#include <QFile>
#include <QDir>
#include <QList>
#include "ui_ConquestBroker.h"
#include <QObject>
//#include <qapplication.h>
#include <iostream>
#include <vector>
#include <sstream>
#include <QDomDocument>
#include <QHeaderView>
#include <QToolButton>
#include <cstdlib>
#include <QtGlobal>
#include <QThread>


#include <QtSql>
#include <QMessageBox>

#ifdef Q_WS_WIN
#include <windows.h>
#include <stdio.h>
#include <tchar.h>
#include <conio.h>
#endif


#ifndef __BrokerApplication_H__
#define __BrokerApplication_H__


class BrokerQueryHost {
public:
	virtual QList<QString> getSelectionAsFiles();
	virtual QDir getNewOutputDir();
	virtual QDir getNewTmpDir();
	virtual void notifyOutputAvailable(QDir outputDir);
	virtual void setStatus(QString status);
};

// Just keep the information in the database don't try to work with it
class BrokerApplication: public QThread {

	Q_OBJECT

	public:	
			BrokerApplication(BrokerApplication *src);
			BrokerApplication(BrokerQueryHost *q, QDomElement xmlEl, QToolButton *s_button);
			bool isEnabled();
			const QString getName();
			const QString getDescription();
			QString getIconPath();
			QString getExePath();
			QList<QString> getAsFiles();
			QDir getOutputDir();
			QDir getTmpDir();
			void run();
			BrokerQueryHost* getQueryHost();
//			QToolButton *button;
//			QLabel *label;

/*
			QString generateUID();
			QRect getAvailableScreen();
			void notifyOutputAvailable(); // Once called search output dir and update output filenames
			void notifyStatus(QString status); // Provide information about the status of this application
			void notifyStateChanged(BrokerState state); // Proivde information about the state of this application
			const QDateTime getTimestamp(); // Get the time at which this application was launched
			int getPID(); // The process ID of this application
*/
			
	private:
			bool enabled;
			QString name, description;
			QString exe_path, icon_path;
			QList<QString> input_filenames;
			QDir output_dir, tmp_dir;
			BrokerQueryHost *qhost;
/*
			QList<QFiles> output_filenames;
			BrokerState state;
			QDateTime timestamp;
			int pid;
*/

	public slots:
		void button_clicked(bool state);

};

#endif


